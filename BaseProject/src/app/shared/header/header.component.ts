import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  title = 'BaseProject';
  constructor(private router: Router) { }
  
  OpenLoginPage(){
    this.router.navigate(['/auth/login']);
  }

  OpenRegisterPage(){
    this.router.navigate(['/auth/register']);
  }
}
