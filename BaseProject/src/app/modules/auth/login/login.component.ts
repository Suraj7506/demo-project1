import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  constructor(private toastr: ToastrService, private router: Router) {}

  ngOnInit() {}

  redirectToRegister() {
    this.router.navigate(['/auth/register']);
  }
  forgotpassword() {
    this.router.navigate(['/auth/forgot-password']);
  }

  redirectToDashboard() {
    this.toastr.success('Login SucessFull!');
    this.router.navigate(['/dashboard']);
  }
}
