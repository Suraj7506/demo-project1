import { Component } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent {

  constructor(private toastr: ToastrService,private router: Router) { }

  generatePassword() {
    // Generate password logic

    // Display toastr message
    this.toastr.success('Password generated successfully!', 'Success');
    this.router.navigate(['/auth/login']);


    // Navigate to the login page
    // You can use Angular Router for navigation, similar to the previous example
  }

}
