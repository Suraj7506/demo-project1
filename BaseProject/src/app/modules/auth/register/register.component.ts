import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent {
  constructor(private toastr: ToastrService, private router: Router) {}

  ngOnInit() {}
  redirectToLogin() {
    this.router.navigate(['/auth/login']);
  }
  redirectToDashboard() {
    this.toastr.success('Register SucessFull!');
    this.router.navigate(['/auth/login']);
  }
}
